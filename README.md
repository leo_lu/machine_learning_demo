install Anaconda for package management
install according to requirements.txt

jupyter notebook set port to 9999
load jupyter-notebook by 
activating virtual environment:
$ source activate "virtual_environment_name"
$ jupyter-notebook

training is done in notebook/Multi Layer Perceptron.ipynb

after training, put test data into "project_folder/data", trained model into "project_folder/model"
run_model.py into "project_folder/src"

run prediction by
	$ python run_model --input_path=<input_path> --model_path=<model_path> --data_header=<Y|N>

	e.g. python run_model.py --input_path=../data/test_data.csv --model_path=../model/my_model.h5 --with_header=Y