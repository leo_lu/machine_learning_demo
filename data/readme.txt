Download raw data from kaggle:
https://www.kaggle.com/mlg-ulb/creditcardfraud
login and "Download" the 69MB zip file

extract the csv file into the following directory
project_directory/data/tidy




A sample test data is available as test.csv in the data folder
it can be tested using
$python project_directory/src/run_model.py --input_path=<input_path> --model_path=<model_path> --data_header=<Y|N>