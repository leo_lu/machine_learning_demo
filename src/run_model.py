'''
	load model and run prediction on a given data
	1. put test data into "project_folder/data/"
	2. put trained model into "project_folder/model"
	3. put this file into "project_folder/src"

	Python version: 3.6 with keras, pandas, numpy, sklearninstalled

	run prediction by

	$ python run_model --input_path=<input_path> --model_path=<model_path> --data_header=<Y|N>

	e.g.
	python run_model.py --input_path=../data/tidy/test_data.csv --model_path=../model/my_model.h5 --with_header=Y
'''

import argparse

from keras.models import load_model
import pandas
import numpy as np

from sklearn.metrics import confusion_matrix

import matplotlib.pyplot as plt

def load_trained_model(model_path):
	return load_model(model_path)

def load_data(input_path, with_header):	# data given as csv with header or without header
	if with_header.lower() in ('yes', 'true', 't', 'y', '1'):
		# data without label header row
		dataframe = pandas.read_csv(input_path, header=1)
	else:
		# data with label header row
		dataframe = pandas.read_csv(input_path, header=0)

	x = dataframe.iloc[:, :-1].values
	y = dataframe.iloc[:, -1].values

	return [x, y]

def evaluate(model, x, given_label, predicted_label):
	print("accuracy: ", model.evaluate(x, given_label))

	print("confusion matrix:")

	labels=['normal', 'fraud']
	cm = confusion_matrix(given_label, predicted_label)
	print(cm)
	fig = plt.figure()
	ax = fig.add_subplot(111)
	cax = ax.matshow(cm)
	plt.title('Confusion matrix of the classifier')
	fig.colorbar(cax)
	ax.set_xticklabels([''] + labels)
	ax.set_yticklabels([''] + labels)
	plt.xlabel('Predicted')
	plt.ylabel('True')
	plt.show()

def main():
	# matplotlib crash fix
	import os
	os.environ['KMP_DUPLICATE_LIB_OK']='True'

	parser = argparse.ArgumentParser(description='Load data and model for prediction')
	parser.add_argument('--input_path', dest='input_path', required=True,
		help='the path to input data')

	parser.add_argument('--model_path', dest='model_path', required=True,
		help='path to model')
	parser.add_argument('--with_header', dest='with_header', required=True,
		help='data with or without header, Y or N')
	args = parser.parse_args()

	print(args)
	print("model path: ", args.model_path)

	model = load_trained_model(args.model_path)

	[x, given_label] = load_data(args.input_path, args.with_header)

	# predict input
	predicted_label = model.predict(x).round()

	evaluate(model, x, given_label, predicted_label)


if __name__ == '__main__':
   	main()
