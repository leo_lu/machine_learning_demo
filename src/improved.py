
# coding: utf-8

# In[2]:


# ref: https://www.kaggle.com/ekababisong/credit-card-fraud-detection-using-xgboost/notebook?scriptVersionId=986452


# In[3]:


# import packages
print(__doc__)
import numpy as np
from scipy import interp
import matplotlib.pyplot as plt
from itertools import cycle
import pandas as pd
from xgboost import XGBClassifier
from sklearn.model_selection import StratifiedKFold
from sklearn.model_selection import cross_val_score
from sklearn.metrics import roc_curve, auc

import keras
from keras.layers import Input, Dense
from keras.models import Model


# In[4]:


# load the data
dataframe = pd.read_csv("../data/creditcard.csv")

print("csv shape: (row: %d, column: %d)" % (dataframe.shape[0], dataframe.shape[1]))


# In[5]:


# auto encoder parameters

# sample autoencoder

train = dataframe[1:284000]
col = dataframe.shape[1]

encoding_dim = 2      # 0 - fraudulent or 1 - normal

test = dataframe[284000:]   # last few hundreds of record for testing


# In[ ]:


# data without headings
"""
print("first 5 train")
print(train[:5])

print ("first 5 test")
print(test[:5])
"""


# In[ ]:


# get column names
colNames = dataframe.columns.values
print(len(colNames), "columns ")  # data with 31 column headings
colNames


# In[ ]:


input_data = Input(shape=(col, ))
encoded = Dense(encoding_dim, activation='relu')(input_data)
decoded = Dense(col, activation='sigmoid')(encoded)

autoencoder = Model(input_data, decoded)

adagrad = keras.optimizers.Adagrad(lr=0.01, epsilon=None, decay=0.0)
autoencoder.compile(optimizer=adagrad, loss="binary_crossentropy")

autoencoder.fit(train, train,
                epochs=50,
                batch_size=16,
#                 shuffle=True,
                validation_data=(test, test))


# In[ ]:


"""
x = Conv2D(32, (3, 3), activation='relu', padding='same')(input_img)
x = MaxPooling2D((2, 2), padding='same')(x)
x = Conv2D(32, (3, 3), activation='relu', padding='same')(x)
encoded = MaxPooling2D((2, 2), padding='same')(x)

# at this point the representation is (7, 7, 32)
x = Conv2D(32, (3, 3), activation='relu', padding='same')(encoded)
x = UpSampling2D((2, 2))(x)
x = Conv2D(32, (3, 3), activation='relu', padding='same')(x)
x = UpSampling2D((2, 2))(x)
decoded = Conv2D(1, (3, 3), activation='sigmoid', padding='same')(x)

autoencoder = Model(input_img, decoded)
autoencoder.compile(optimizer='adadelta', loss='binary_crossentropy')
"""


# In[ ]:


# get dataframe dimensions
print ("Dimension of dataset:", dataframe.shape)


# In[ ]:


# get attribute summaries
print(dataframe.describe())


# In[ ]:


# get class distribution
print ("Normal transaction:", dataframe['Class'][dataframe['Class'] == 0].count()) #class = 0
print ("Fraudulent transaction:", dataframe['Class'][dataframe['Class'] == 1].count()) #class = 1


# In[ ]:


# separate classes into different datasets
class0 = dataframe.query('Class == 0')
class1 = dataframe.query('Class == 1')

# randomize the datasets
class0 = class0.sample(frac=1)
class1 = class1.sample(frac=1)


# In[ ]:


# undersample majority class due to class imbalance before training - train
class0train = class0.iloc[0:6000]
class1train = class1

# combine subset of different classes into one balaced dataframe
train = class0train.append(class1train, ignore_index=True).values


# In[ ]:


# split data into X and y
X = train[:,0:30].astype(float)
Y = train[:,30]


# In[ ]:


# XGBoost CV model
model = XGBClassifier()
kfold = StratifiedKFold(n_splits=10, random_state=7)

# use area under the precision-recall curve to show classification accuracy
scoring = 'roc_auc'
results = cross_val_score(model, X, Y, cv=kfold, scoring = scoring)
print( "AUC: %.3f (%.3f)" % (results.mean(), results.std()) )


# In[ ]:


# change size of Matplotlib plot
fig_size = plt.rcParams["figure.figsize"] # Get current size

old_fig_params = fig_size
# new figure parameters
fig_size[0] = 12
fig_size[1] = 9

plt.rcParams["figure.figsize"] = fig_size # set new size


# In[ ]:


# plot roc-curve
# code adapted from http://scikit-learn.org
mean_tpr = 0.0
mean_fpr = np.linspace(0, 1, 100)

colors = cycle(['cyan', 'indigo', 'seagreen', 'yellow', 'blue', 'darkorange'])
lw = 2

i = 0
for (train, test), color in zip(kfold.split(X, Y), colors):
    probas_ = model.fit(X[train], Y[train]).predict_proba(X[test])
    # Compute ROC curve and area the curve
    fpr, tpr, thresholds = roc_curve(Y[test], probas_[:, 1])
    mean_tpr += interp(mean_fpr, fpr, tpr)
    mean_tpr[0] = 0.0
    roc_auc = auc(fpr, tpr)
    plt.plot(fpr, tpr, lw=lw, color=color,
             label='ROC fold %d (area = %0.2f)' % (i, roc_auc))

    i += 1
plt.plot([0, 1], [0, 1], linestyle='--', lw=lw, color='k',
         label='Luck')

mean_tpr /= kfold.get_n_splits(X, Y)
mean_tpr[-1] = 1.0
mean_auc = auc(mean_fpr, mean_tpr)
plt.plot(mean_fpr, mean_tpr, color='g', linestyle='--',
         label='Mean ROC (area = %0.2f)' % mean_auc, lw=lw)

plt.xlim([-0.05, 1.05])
plt.ylim([-0.05, 1.05])
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('Receiver operating characteristic example')
plt.legend(loc="lower right")
plt.show()
